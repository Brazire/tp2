function swapImg(){
    if(window.innerWidth > 750)
        document.getElementById("article2").parentNode.insertBefore(document.getElementById("article2"),  document.getElementById("article2").parentNode.firstChild);

    else
        document.getElementById("img2").parentNode.insertBefore(document.getElementById("img2"),  document.getElementById("img2").parentNode.firstChild);
}

window.addEventListener('load', swapImg);
window.addEventListener('resize', swapImg);